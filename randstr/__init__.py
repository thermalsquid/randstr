import click
from . import lib

@click.command(context_settings=dict(help_option_names=['-h', '--help']))
@click.argument('length', type=int)
@click.option('-a', help="Use alphabetical characters", is_flag=True)
@click.option('-n', help="Use numerical characters", is_flag=True)
@click.option('-C', 'case', flag_value='upper', help="Use only uppercase letters")
@click.option('-c', 'case', flag_value='lower', help="Use only lowercase letters")
def cli(length, a, n, case):

    """

        randstr - A random string generator

        Randstr is a simple program that generates a random string of an arbitrary length with random characters. Uses the cryptographically strong python library `secrets` for random number generation and the `Click` package for the command line interface.

    """

    click.echo(click.style(lib.generate_random_string(length, a, n, case), fg='green'))

if __name__ == "__main__":
    cli()
