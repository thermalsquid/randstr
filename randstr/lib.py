import sys
import secrets
import string

def generate_random_string(length, a, n, case=None):

    """

    Generate a random string.

    length -- String length
    a -- Include alphabetical characters
    n -- Include numerical characters
    upper -- Output in uppercase (optional)
    lower -- Output in lowercase (optional)

    """

    alphabetical = a
    numerical = n
    chars = ""
    password = ""

    if alphabetical:
        chars += string.ascii_letters
    if numerical:
        chars += string.digits
    
    if chars == "":
        chars = string.ascii_letters + string.digits + string.punctuation

    for i in range(length):
        password += secrets.choice(chars)

    if case is not None:
        if case == 'upper':
            password = password.upper()
        elif case == 'lower':
            password = password.lower()
        else:
            print("Case must be either 'upper' or 'lower'.")
            return

    return password
