import pytest
import click

from randstr.lib import generate_random_string

iter_count = 30

#TODO: Test Click application
# Tests each assertion multiple times to get a good sample range since output is random

def test_is_generate_string():

    for i in range(iter_count):
        assert type(generate_random_string(16, True, False)) is str

def test_is_generate_alphabetic():

    for i in range(iter_count):
        assert generate_random_string(16, True, False).isalpha()

def test_is_generate_numeric():

    for i in range(iter_count):
        assert generate_random_string(16, False, True).isnumeric()

def test_is_generate_upper():

    for i in range(iter_count):
        assert generate_random_string(16, True, False, case='upper').isupper()
        assert not generate_random_string(16, True, False, case='lower').isupper()
        assert not generate_random_string(16, False, True).isupper()
