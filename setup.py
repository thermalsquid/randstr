try:
    from setuptools import setup, find_packages
except ImportError:
    from distutils.core import setup

setup(
    name='randstr',
    version='0.6',
    description='A random string generator for the terminal',
    author='Brandon Bang',
    author_email='brandonbang@protonmail.com',
    url='https://gitlab.com/thermalsquid/randstr',
    packages=[
        'randstr'
    ],
    include_package_data=True,
    install_requires=[
        'click'
    ],
    tests_require=[
        'pytest'
    ],
    entry_points='''
        [console_scripts]
        randstr=randstr:cli
        ''',
    classifiers=(
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Intended Audience :: End Users/Desktop'
    ),
)