# randstr - A random string generator

Randstr is a simple program that generates a random string of an arbitrary length with random characters. Options include alphanumeric only, alphabetical only, and numerical only, and upper and lowercase. Uses the cryptographically strong python library `secrets` for random number generation and the `Click` package for the command line interface.

## Examples:
```bash
    # Default with a length of 8 special and alphanumeric characters
    $ randstr 8
    $ ~@sTA0ZF

    # Only alphanumeric characters
    $ randstr 8 -an
    $ R1c63ET1

    # Only letters
    $ randstr 8 -a
    $ rXyMBlJt

    # Only numbers
    $ randstr 8 -n
    $ 15018412

    # Upper case letters
    $ randstr 8 -Ca
    $ FCSBCKMB

    # Lower case letters
    $ randstr 8 -ca
    $ chikrqlc
```

## How to Install:
1. After downloading and unzipping or cloning, open a terminal and go to the directory that contains `randstr-master`.
2. `$ pip3 install randstr-master/`
You should now be able to use `randstr` from anywhere on your terminal!

## Future plans:
- Add to PyPI
- Make more advanced character selection available
